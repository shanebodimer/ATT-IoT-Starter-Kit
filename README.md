# AT&T IoT Starter Kit Documentation

The following documentation is a custom quick start guide to get your AT&T IoT Starter Kit up and running.

# Jump To

* [Quick Start](#quick-start)
* [Lessons Learned](#lessons-learned)

# Authors

* Shane Bodimer
* Payton Lenz

# Quick Start

The following steps are a simplified version of the instructions found here: [https://s3-us-west-2.amazonaws.com/starterkit-assets/StarterKitGuide-V1.2.pdf](https://s3-us-west-2.amazonaws.com/starterkit-assets/StarterKitGuide-V1.2.pdf)

1. **Sim Activation**

    1.1. Visit [https://starterkit.att.com/activate](https://starterkit.att.com/activate)

    1.2. Create an account (We used our existing @att.com accounts)

    1.3. Follow the given steps for activating your sim card. (It may take a few minutes for you account to update to show you your sim card on this account) Keep this tab open.

    1.4  When the sim is activated it should take you through the process to create an At&t Control Center account. If it doesn't, expand the details of the sim card using the plus sign on the right and click on the "Launch AT&T Control Center" in the bottom right.

    1.5  Complete the account creation process, log in , and go to the devices tab. You should see you sim card there.

    1.6. Keep this tab open and continue with the following steps

2. **Board Assembly**

    2.1. Screw on both antennas

    2.2. Attach board labeled WNC to the top of the Arduino board

    2.3. The WNC board's pins should line up with the outer pin-holes on the Arduino

    2.4. Insert pins and push down until only a small gap remains

    2.5. Connect a micro-usb cable to the top right slot on the WNC board (right of the SIM opening), then connect the other end to a power outlet

    2.6. Connect another micro-usb cable to the bottom left slot on the Arduino (left of the ethernet opening), then connect the other end to your computer

3. **Check Device Version**

    3.1. With your device plugged into a computer, navigate to it's location with your file browsers

    3.2. Find and open in a text editor either ``DETAILS.TXT`` or ``MBED.htm``

    3.3. Check and see if version is number is greater than or less than 0226

    3.4. If the version number is less than 0226, proceed to step 8, otherwise, skip to step 9

4. **Updating Device**

    4.1. Follow the steps here: [https://developer.mbed.org/handbook/Firmware-FRDM-K64F](https://developer.mbed.org/handbook/Firmware-FRDM-K64F)

    4.2. When downloading updated firmware, select the most recent version: *DAPLink rev0243*

5. **Flow Setup**

    5.1. Visit [https://flow.att.io/starter-kit-core/starter-kit-base/home](https://flow.att.io/starter-kit-core/starter-kit-base/home)

    5.2. Fork the Flow (make sure you are logged in)

    5.3. Keep this tab open and continue with the following steps

6. **M2X**

    6.1. Visit [https://m2x.att.com](https://m2x.att.com)

    6.2. While logged in, look to the upper right corner and click *Manage*, then *Account Settings*

    6.3. On the *Account Settings* page, copy your *Master Key*

    6.4. Return to Flow

7. **Flow Configuration**

    7.1. Once back to your Flow document, open the *Configuration Function* at the top of the Flow

    7.2. On the third line of the configuration function, paste your M2X master key

    7.3. Exit the configuration node and check *Full Build* then *Deploy*

    7.4. After your build is deployed, open the *Endpoints* tab at the bottom of the Flow

    7.6. Copy your *Base URL*, then return to the *Configuration Function*

    7.7. In the configuration function, paste the beginning of your Base URL into the *base_hostname* variable (looks like this: ``runm-central.att.io``)

    7.8. In the configuration function, paste the end of your Base URL into the *base_uri* variable (looks like this: ``/3624c79a99d69/dfcf549825a4/9057a2b0a8ea355/in/flow``)

    7.9. Exit the configuration node

8. **Creating the Device**

    8.1. While still in Flow, click on the *Virtual Device* tab near the top

    8.2. Towards the bottom of the Flow, inject the *Startup* node

    8.3. In the *Debug Console*, you should see confirmation that your device was created

9. **Software Update**

    9.1. In a new tab, visit [https://developer.mbed.org/platforms/FRDM-K64F/](https://developer.mbed.org/platforms/FRDM-K64F/)

    9.2. Create an account

    9.3. Click *Add to your mbed compiler*

10. **MBED Configuration**

    10.1. Visit [https://developer.mbed.org/teams/Avnet/code/Avnet_ATT_Cellular_IOT/](https://developer.mbed.org/teams/Avnet/code/Avnet_ATT_Cellular_IOT/)

    10.2. Click *Import into Compiler*

    10.3. Follow through with the import instructions, no need to change any names

    10.4. Once imported, click on the ``config_me.h`` file on the left

11. **Getting Configuration Info**

    11.1. Go back to your Flow tab and click on the *Data* tab

    11.2. Open the *Endpoints* tab again and record your *Base URL*

    11.3. Go back to your M2X tab and view all your devices

    11.4. Click on your Starter Kit device and record your *Device Serial*

    11.5. Return to your MBED Configuration tab

12. **MBED Configuration**

    12.1. Paste your Base URL and Device Serial into the ``config_me.h`` file where

    12.2. Save the ``config_me.h`` file

    12.3. Click the *Compile* button, a file should be downloaded

13. **Deploy Device**

    13.1. Unplug your device from all outlets and computers, wait 20 seconds

    13.2. Plug your device back into all outlets and computers

    13.3. Drag the recently downloaded file from MBED to your device file

    13.4. Wait 30 seconds, then unplug your device from all outlets and computer

    13.5. Wait 20 seconds, then plug your device back into all outlets and computers

    13.6 A light on the device will be bright red for a minute or so and then will change colors and eventually stop on green.

14. **Activate Sim**

    14.1. Return to your sim tab in the At&t Starter Kit Account and change the status of your sim card to *Activated*

15. **Wait**

After a while, your device should be online and feeding data into Flow and then into M2X. Visit your Flow or M2X tabs to check.

Check to see if your red light on your device has turned green.


## Lessons Learned

* Shane: I think the provided instructions were clear and right to the point, however, I learned nothing. The guide did not do a good job explaining what *exactly* I was doing. Yes it gave me instructions to get the device up and running, but I never understood how it worked

* Payton: Once you found the updated directions and had all of the accounts set up correctly it went smoothly. The directions included in the box were out of date. Also, a lot of the links were broken and weren't linked to the right documents and you had to search for the proper files. The biggest issue for me was that M2X and Flow sites were down for about 3 hours and I couldn't get my accounts set up correctly. After that it came together fairly quickly. I agree with Shane when he says it wasn't a *learning* tool, it was more just to get the device to work. There wasn't much there when it came to actual educational information on M2X and Flow.

This Sucked!!
